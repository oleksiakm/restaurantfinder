## Overview

**RestaurantFinder** was created using ASP.NET Core and VueJs (https://vuejs.org/) 


## How to run RestaurantFinder.Web

1. Got to **Restaurant.Web** directory.
2. Run **npm install** command to download all dependencies.
3. Run **dotnet run** command to build and run application.
4. After successful build the url to access running application will be presented on the console.

## Dependencies

In order to properly run **RestaurantFinder.Web** you will need following dependencies installed:

* .NET Core (https://www.microsoft.com/net/learn/get-started/windows)
* npm (https://www.npmjs.com/get-npm)