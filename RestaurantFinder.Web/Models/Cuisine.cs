﻿namespace RestaurantFinder.Web.Models
{
    public enum Cuisine
    {
        Polish,
        Italian,
        French
    }
}
