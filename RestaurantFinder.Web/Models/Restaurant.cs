﻿namespace RestaurantFinder.Web.Models
{
    public class Restaurant
    {
        public string Name { get; set; }
        public Cuisine Cuisine { get; set; }
        public decimal MinimumOrderCost { get; set; }
        public decimal MaximumDeliveryCost { get; set; }
        public State State { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}