﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import { Watch } from 'vue-property-decorator';
import $ from 'jquery';
import { EventBus } from '../../event-bus'
import { RestaurantFilter, Cuisine, State } from '../../models'


interface Restaurant {
    name: string,
    cuisine: Cuisine,
    minimumOrderCost: number,
    maximumDeliveryCost: number,
    state: State,
    latitude: number,
    longitude: number
}

@Component
export default class RestaurantsComponent extends Vue {

    restaurantsData: Restaurant[] = [];

    mapWidth: number;
    mapHeight: number;

    cuisineToMakerClassMap: { [id: number]: string } = {};    

    mounted() {

        this.search();

        this.mapWidth = (this.getMap().width() as number) - 40;
        this.mapHeight = (this.getMap().height() as number) - 20;

        this.cuisineToMakerClassMap[Cuisine.Italian] = "italian";
        this.cuisineToMakerClassMap[Cuisine.French] = "french";
        this.cuisineToMakerClassMap[Cuisine.Polish] = "polish";

        let self = this;
        EventBus.$on("filterChanged", function (data: RestaurantFilter) {
            self.search(data);
        });
    } 

    search(filter?: RestaurantFilter) {
        let url = "api/restaurants";
        if (filter) {
            let params = this.getQueryParams(filter);
            url += "?" + params;
        }
        
        this.fetchRestaurants(url);
    }

    fetchRestaurants(url: string) {
        fetch(url)
            .then(response => response.json() as Promise<Restaurant[]>)
            .then(data => {
                this.restaurantsData = data;
            });
    }

    @Watch("restaurantsData")
    onRestaurantsChanged(value: Restaurant[], oldValue: Restaurant[]) {
        this.clearMap();

        for (var i = 0; i < value.length; i++) {
            this.drawRestaurantOnMap(value[i]);
        }
    }

    drawRestaurantOnMap(data: Restaurant) {
        let markerClass = this.cuisineToMakerClassMap[data.cuisine];

        this.drawRestaurantMaker(data.longitude * 5, data.latitude * 3.5, markerClass, data.state !== State.Open);
    }  

    drawRestaurantMaker(x: number, y: number, cssClass: string, closed: boolean) {
        let marker = $("<div/>").addClass(cssClass).css({
            "left": x + "px",
            "top": y + "px"
        });

        if (closed) {
            marker.addClass("closed");
        }

        this.getMap().append(marker);
    }

    getMap() {
        return $("#restaurant-map")
    }

    clearMap() {
        this.getMap().empty();
    }

    getQueryParams(obj: any) {
        let params = [];
        for (let key in obj) {
            if (obj.hasOwnProperty(key) && obj[key] !== null) {
                params.push(key + "=" + encodeURIComponent(obj[key]));
            }
        }

        return params.join("&");
    }
}

