﻿import Vue from 'vue';
import { Component, Watch } from 'vue-property-decorator';
import { EventBus } from '../../event-bus'
import { RestaurantFilter, Cuisine } from '../../models'

@Component
export default class RestaurantFilterComponent extends Vue {

    filter: RestaurantFilter = {
        cuisine: null,
        minOrderCost: null,
        maxDeliveryCost: null,
        open: false
    };

    cuisineOptions = [
        { text: "All", value: null },
        { text: "Polish", value: Cuisine.Polish },
        { text: "Italian", value: Cuisine.Italian },
        { text: "French", value: Cuisine.French }
    ];    
   
    @Watch("filter", { immediate: false, deep: true })
    onFilterChange(value: RestaurantFilter, oldValue: RestaurantFilter) {
        EventBus.$emit("filterChanged", value);
    }
}