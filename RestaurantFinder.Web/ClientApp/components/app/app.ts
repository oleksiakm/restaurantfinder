import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component({
    components: {
        Restaurants: require('../restaurants/restaurants.vue.html'),
        RestaurantFilter: require('../restaurantFilter/restaurantFilter.vue.html')
    }
})
export default class AppComponent extends Vue {
}
