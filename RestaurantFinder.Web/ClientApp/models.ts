﻿export enum Cuisine {
    Polish,
    Italian,
    French
}

export enum State {
    Open,
    Closed
}

export interface RestaurantFilter {
    cuisine: Cuisine | null;
    minOrderCost: number | null;
    maxDeliveryCost: number | null;
    open: boolean;
}


