﻿using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using RestaurantFinder.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantFinder.Web.Controllers
{
    public class RestaurantsController : Controller
    {
        private static IEnumerable<Restaurant> _restaurants;
        static RestaurantsController()
        {
            var fixture = new Fixture();
            _restaurants = fixture.CreateMany<Restaurant>(40);
        }

        [Route("api/restaurants")]
        public IEnumerable<Restaurant> GetRestaurants(
            [FromQuery] Cuisine? cuisine, 
            [FromQuery] decimal? minOrderCost, 
            [FromQuery] decimal? maxDeliveryCost,
            [FromQuery] bool? open)
        {
            var restaurants = _restaurants;

            if (cuisine.HasValue)
            {
                restaurants = restaurants.Where(x => x.Cuisine == cuisine.Value);
            }

            if(minOrderCost.HasValue)
            {
                restaurants = restaurants.Where(x => x.MinimumOrderCost <= minOrderCost.Value);
            }
            
            if (maxDeliveryCost.HasValue)
            {
                restaurants = restaurants.Where(x => x.MaximumDeliveryCost <= maxDeliveryCost.Value);
            }

            if(open.HasValue && open.Value)
            {
                restaurants = restaurants.Where(x => x.State == State.Open);
            }

            return restaurants;
        }
    }
}
